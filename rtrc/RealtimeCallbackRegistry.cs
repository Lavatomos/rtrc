using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace RtRC {
    public delegate void RealtimeCallback(IRealtimeModel[] data, RealtimeUser user);

    public class RealtimeCallbackRegistry {
        private Dictionary<int, IRealtimeModel> models;
        private Dictionary<Type, List<RealtimeCallback>> modelCallbacks;

        public RealtimeCallbackRegistry() {
            models = new Dictionary<int, IRealtimeModel>();
            modelCallbacks = new Dictionary<Type, List<RealtimeCallback>>();
        }

        public void RegisterModel(IRealtimeModel model) {
            models.Add(model.ModelIdentifier(), model);
            modelCallbacks.Add(model.GetType(), new List<RealtimeCallback>());
        }

        public IRealtimeModel GetModel(int modelIdentifier) {
            return models[modelIdentifier];
        }

        public void RegisterCallback<T>(RealtimeCallback callback) where T : IRealtimeModel {
            if (!modelCallbacks.ContainsKey(typeof(T))) throw new KeyNotFoundException("Model not registered!");
            modelCallbacks[typeof(T)].Add(callback);
        }

        public void TriggerCallbacks(int modelIdentifier, byte[] dataBlob, RealtimeUser user) {
            var model = GetModel(modelIdentifier);
            var callbacks = modelCallbacks[model.GetType()];
            var modelInstances = DissectDataBlob(dataBlob, model);

            foreach (var callback in callbacks) {
                callback(modelInstances, user);
            }
        }

        private IRealtimeModel[] DissectDataBlob(byte[] dataBlob, IRealtimeModel model) {
            var totalModelCount = (dataBlob.Length / model.ModelLength());
            var modelLength = model.ModelLength();
            var modelArray = new IRealtimeModel[totalModelCount];
            for (int i = 0; i < totalModelCount; i++) {
                byte[] modelBytes = dataBlob.Skip(i * modelLength).Take(modelLength).ToArray();
                modelArray[i] = model.FromBytes(modelBytes);
            }

            return modelArray;
        }

    }
}